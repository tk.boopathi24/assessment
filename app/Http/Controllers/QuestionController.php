<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\UserScore;
use Symfony\Component\HttpFoundation\Response;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;


class QuestionController extends Controller
{
    public function index()
    {
        //
    }



    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(request $data)
    {
        $user = Auth::user();
        try{
        DB::transaction(function () use ($data,$user) {

            if($data->id){
                $question = Question::where('id', $data->id)->first();
            }else{
                $question = new Question;
            }
            $question->question = $data->question;
            $question->option1 = $data->option1;
            $question->option2 = $data->option2;
            $question->option3 = $data->option3;
            $question->option4 = $data->option4;
            $question->answer = $data->answer;
            $question->created_by = $user->id;
            
            $question->save();
            
            });        
        }catch(\Exception $e){

                Log::error("Error: ".$e);
                Log::info("Line Number: ".$e->getLine());

                return false;

              }        
    }

   
        public function list(Request $request){
       $questions = Question::all();
        return Datatables::of($questions)
            ->addColumn('actions', function ($questions) {
                $buttons ="";
                $buttons = '<p ><button ng-click="editQuestion(' . $questions->id . ')" '
                    . 'class="btn btn-info btn-rounded btn-sm">'
                    . '<span class="fa fa-edit">Edit</span></button>'
                    . '<button class="btn btn-danger btn-rounded btn-sm mb-control" '
                    . 'data-box="#message-box-danger" ng-click="deleteQuestion(' . $questions->id . ')">'
                        . '<span class="fa fa-times">Delete</span></button>';
                return $buttons;
            })
            ->rawColumns(['actions'])
            ->make(true);
        }

    public function show($id)
    {
        $question = Question::find($id);
        return $question;

    }

    public function getQuestion(){
        $questions = Question::select('id','question','option1','option2','option3','option4')->get();
        return $questions;
    }
    
    public function formSubmit(Request $request){
            $totalCorrectAnswer=0;
            $datas = array_filter($request->answer);
            $totalQuestCount = Question::count();
        try{            
            foreach($datas as $key=>$value) {
                $ValidateResponse = Question::select('answer')->find($key);
                if($value == $ValidateResponse->answer){
                    $totalCorrectAnswer++;
                }
            }        
            $status ="FAILED";
            if($totalCorrectAnswer!=0){
                $average = ($totalCorrectAnswer*100)/$totalQuestCount;
                if($average>=80){
                    $status ="PASS";    
                }
            }
            $userScore = new UserScore;
            $userScore->user_id = Auth::user()->id;
            $userScore->result = $status;
            $userScore->token = "tok_".uniqid();
            $userScore->save();
                return response()->json([$status],200,[]);
            }catch(\Exception $e){

                Log::error("Error: ".$e);
                Log::info("Line Number: ".$e->getLine());

                return false;

              }             
        }
    
    public function destroy($id)
    {
        try {
            Question::destroy($id);
             return response()->json(['Question Deleted Successfully!'],200,[]);
        } catch (Exception $e) {
            Log::error("Error: ".$e->getMessage());
            Log::info("Line Number: ".$e->getLine());            
            return $this->respondInternalError('Sorry. Something went wrong');
        }
    }
}
