<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserScore extends Model
{
    protected $fillable = [
        'token', 'question_id', 'answer_id','user_id',
    ];
}
