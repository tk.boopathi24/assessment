<!DOCTYPE html>

<html lang="en">
    <head>
        <!-- META SECTION -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Assessment</title>
        <title>{{ config('app.name', 'Assessment') }}</title>

        <!-- start: Css -->

<link rel="stylesheet" type="text/css" id="theme"
        href="{{asset('asset/css/theme-default.css')}}"/>
        <!-- plugins -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @yield('pageStyle')

        <!-- end: Css -->
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
         <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    </head>
    <body>
        <div class="page-container {{ Session::get('sidebarState') }}">
