</div>
<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to log out?</p>
                <p>Press No if you want to continue working. Press Yes to logout.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="{{ route('logout') }}" class="btn btn-success btn-lg">Yes</a>
                    <button class="btn btn-default btn-lg"  data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->
<!-- start: Javascript -->


    <!-- START SCRIPTS -->

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>




<script type="text/javascript" src="{{ asset('asset/js/plugins/jquery/jquery.min.js')}}"></script>

<script type="text/javascript" src="{{ asset('asset/js/plugins/jquery/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('asset/js/plugins/bootstrap/bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.1/angular-sanitize.min.js"></script>


        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->


    <!-- custom -->
    <script type="text/javascript">
    $('.x-navigation-minimize').on('click', function(e) {
        $.ajax({
            type: "GET",
            url: "{{ url('savestate') }}"
        });
    });
    </script>

<!-- end: Javascript -->

@stack('scripts')
</body>
</html>
