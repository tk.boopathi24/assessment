<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Alert</div>
            <div class="mb-content">
                <p>Are you sure to delete?</p>
            </div>
            <div class="mb-footer">
                    <div class="pull-right">
                        <button ng-click="deleteConfirmed()" class="btn btn-success btn-lg">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
            </div>
        </div>
    </div>
</div>
