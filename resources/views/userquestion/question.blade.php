@extends('questlayouts.app')

@section('contentSection')  

       <main class="py-2">    
            <div class="container" >
                <div class="col-md-12" ng-app="assessmentFormApp" ng-controller="assessmentFormController">

                      <div class="card">
                            <div class="card-header">Assessment
                                <a href="#" name="logout" style="color:#000" class="pull-right mb-control" ng-click="logOut()" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
                            </div>

                                    <div raw-ajax-busy-indicator class="bg_load text-center" ng-show="loading"
                                           id="loading-block">
                                          <img src="{{asset('asset/img/ajax.gif')}}" style="margin-top:25%;">
                                      </div>

                            <div class="card-body">
                                <form class="form-horizontal" ng-submit="submitAnswer()" ng-model="assessmentForm" method="post">
                <div class="modal-body modal-height-0">
                    <div class="alert alert-danger" role="alert" ng-if="modalErrors.length > 0">
                        <a class="close" aria-label="Close" ng-click="hideMessage()">&times;</a>
                        <ul><li ng-repeat="modalError in modalErrors">@{{modalError}}</li></ul>
                    </div>
                    <div class="col-md-8" ng-repeat="assessment in assessmentForm">
                        <ul style="list-style: none">
                            <li><strong>@{{$index+1}}. </strong>  <strong ng-bind="assessment.question"></strong></li>
                            <li>&nbsp;&nbsp;&nbsp;
                                  <label style="font-weight:300" class="col-md-8 col-xs-12">
                                <input type="radio" name="answers_@{{$index+1}}" ng-model="questionsForm.answer[assessment.id]" checked
                                ng-value="assessment.option1">
                                        a. @{{assessment.option1}}</span>
                                    </label>    
                            </li>
                            <li>
                                  <label style="font-weight:300" class="col-md-8 col-xs-12">
                                <input type="radio" name="answers_@{{$index+1}}" ng-model="questionsForm.answer[assessment.id]" 
                                ng-value="assessment.option2">
                                    b.  @{{assessment.option2}}</span>
                                    </label>    
                            </li>            
                            <li>&nbsp;&nbsp;&nbsp;
                                  <label style="font-weight:300" class="col-md-8 col-xs-12">
                                <input type="radio" name="answers_@{{$index+1}}" ng-model="questionsForm.answer[assessment.id]" 
                                ng-value="assessment.option3">
                                        c. @{{assessment.option3}}</span>
                                    </label>    
                            </li>                               
                                <li>&nbsp;&nbsp;&nbsp;
                                  <label style="font-weight:300" class="col-md-8 col-xs-12">
                                <input type="radio" name="answers_@{{$index+1}}" ng-model="questionsForm.answer[assessment.id]" 
                                ng-value="assessment.option4">
                                        d. @{{assessment.option4}}</span>
                                    </label>    
                            </li>            
                        </ul>
                       
                       
                    </div>
                    <div class="col-md-12">
                    <button type="submit" id="submit-mb" class="btn btn-primary pull-right">Submit</button>                        
                    </div>
                   

                <div class="modal fade" id="mb-status" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @{{popupHeader}}
            </div>
            
                <div class="modal-body">
                  @{{popupContent}}  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" ng-click="redirectHome()">Ok</button>
                </div>                
        </div>
    </div>
</div>  
            </form>
    
        </div>
    </div>    

                  




 </div>
</div> 
</main>
    
@endsection

@push('scripts')
    <script type="text/javascript" src="{{asset('asset/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
    <script type="text/javascript" src="{{asset('asset/js/userassessment.js')}}"></script>
    <script type = "text/javascript" src="{{asset('asset/js/loader.js')}}"></script>
@endpush