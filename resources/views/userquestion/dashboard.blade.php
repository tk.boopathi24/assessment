@extends('questlayouts.app')

@section('contentSection')  

       <main class="py-2">    
            <div class="container" >
                <div class="col-md-12" >

                      <div class="card">
                            <div class="card-header">Dashboard
                                <a href="#" name="logout" style="color:#000" class="pull-right mb-control" ng-click="logOut()" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
                            </div>

                            <div class="card-body">

                             <div class="modal-body modal-height-0">
                    
                                    <span>This test has been designed to test your skill for Chain Rules. You will be presented Multiple Choice Questions (MCQs) based on Chain Rules Concepts, where you will be given four options. You will select the best suitable answer for the question and then proceed to the next question without wasting given time. You will get your online test score after finishing the complete test.</span>                                
                       
                        </div>
                    <div class="col-md-12">
                        <a href="/home"  class="btn btn-primary pull-right">Take Test </a>
                    
                    </div>
                   

      </form>
    
        </div>
    </div>    

                  




 </div>
</div> 
</main>
    
@endsection

