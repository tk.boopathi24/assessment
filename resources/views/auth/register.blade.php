<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
   
   <script src="{{ asset('js/app.js') }}" defer></script>
  
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
     /*Loader starts */
     .error-box{
        border: 1px solid  #F83D1C;
     }   
    .is-danger        {
        color: #8B0000;
    }
  .bg_load {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      background: #000;
      opacity:0.4;
      z-index:9999;
  }
  /*Loader ends */

</style>
</head>
    <body>
        <main class="py-4">    
            <div class="container" ng-app="registrationApp">
                <div class="row justify-content-center" ng-controller="registrationController">
                    <div class="col-md-8">

                   


                        <div class="card">
                            <div class="card-header">{{ __('Register') }}</div>
                        <div raw-ajax-busy-indicator class="bg_load text-center" ng-show="loading"
                               id="loading-block">
                              <img src="{{asset('asset/img/ajax.gif')}}" style="margin-top:25%;">
                          </div>
                            <div class="card-body">
                                <form ng-submit="applicantFormSubmit()" ng-model="applicantForm" method="post">

                                    @csrf



                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="name"
                                         ng-model="applicantForm.name" ng-class="{'error-box': getError('name')}" >

                                        <span class="help-block is-danger"> @{{getError('name')}}</span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                        <div class="col-md-6">
                                              <input type="text" name="email"
                                              class="form-control"
                                              ng-model="applicantForm.email" ng-class="{'error-box': getError('email')}" />
                                              <span class="help-block is-danger"> @{{getError('email')}}</span>                             
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>

                                        <div class="col-md-6">
                                              <select class="form-control" name="role"
                                                id="role" ng-model="applicantForm.role" ng-class="{'error-box': getError('role')}" >
                                                <option value="">Select Role</option>
                                                <option value="admin">Admin</option>
                                                <option value="user">User</option>
                                              </select>                                
                                            <span class="help-block is-danger"> @{{getError('role')}}</span>
                                        </div>
                                    </div>                        

                                    <div class="form-group row">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password"ng-model="applicantForm.password" ng-class="{'error-box': getError('password')}"
                                            >

                                                <span class="help-block is-danger"> @{{getError('password')}}</span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control" 
                                            ng-model="applicantForm.password_confirmation" ng-class="{'error-box': getError('password_confirmation')}"
                                            name="password_confirmation" >
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>    

<script type="text/javascript" src="{{ asset('asset/js/plugins/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('asset/js/plugins/jquery/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('asset/js/plugins/bootstrap/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.9/angular.min.js"></script>

<script type = "text/javascript" src="{{asset('asset/js/register.js')}}"></script>
<script type = "text/javascript" src="{{asset('asset/js/loader.js')}}"></script>
    </body>
</html>