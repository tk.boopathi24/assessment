<div class="modal fade" id="mb-addquestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@{{ questionsForm.id ? "Edit":"New" }}</h5>
            </div>
            <form class="form-horizontal" ng-submit="submitQuestion()" ng-model="questionsForm" method="post">
                <div class="modal-body modal-height-0">
                    <div class="alert alert-danger" role="alert" ng-if="modalErrors.length > 0">
                        <a class="close" aria-label="Close" ng-click="hideMessage()">&times;</a>
                        <ul><li ng-repeat="modalError in modalErrors">@{{modalError}}</li></ul>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-xs-12">                                            
                            <div class="input-group">
                                <input type="text" class="form-control" ng-model="questionsForm.question" placeholder="Enter Question" style="width:100%">
                            </div>                                            
                        </div>
                    </div>

                    <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">
                        <input type="radio" name="answers" ng-model="questionsForm.answer" checked
                        ng-value="questionsForm.option1"></label>
                        <div class="col-md-6 col-xs-12">
                            <div class="input-group">
                                <input type="text" class="form-control" ng-model="questionsForm.option1" placeholder="Answer 1">
                            </div>                                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">
                        <input type="radio" name="answers" ng-model="questionsForm.answer" 
                        ng-value="questionsForm.option2">
                    </label>
                        <div class="col-md-6 col-xs-12">
                            <div class="input-group">
                                <input type="text" class="form-control" ng-model="questionsForm.option2" placeholder="Answer 2">
                            </div>                                            
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">
                        <input type="radio" name="answers" ng-model="questionsForm.answer" 
                        ng-value="questionsForm.option3">
                    </label>
                        <div class="col-md-6 col-xs-12">
                            <div class="input-group">
                                <input type="text" class="form-control" ng-model="questionsForm.option3" placeholder="Answer 3">
                            </div>                                            
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">
                        <input type="radio" name="answers" ng-model="questionsForm.answer" 
                        ng-value="questionsForm.option4">
                    </label>
                        <div class="col-md-6 col-xs-12">
                            <div class="input-group">
                                <input type="text" class="form-control" ng-model="questionsForm.option4" placeholder="Answer 4">
                            </div>                                            
                        </div>
                    </div>                    
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit-mb" class="btn btn-primary pull-right">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

