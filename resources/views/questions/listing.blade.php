@extends('questlayouts.app')

@section('contentSection')  
<div ng-cloak ng-app="questionApp" ng-controller="questionController">
                
                        <a href="#" name="logout" style="color:#000" class="pull-right mb-control" ng-click="logOut()" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                <!-- PAGE TITLE -->
                <div class="page-title">              
                    <div class="pull-right">
                        <h2><a href="#" class="mb-control"  ng-click="openModal()" data-box="#mb-addquestion"><span class="fa fa-plus-circle"></span></a></h2>
                    </div>
                    <div class="pull-left">
                        <h2 style="color:#000"><a href="javascript:history.back()"><span class="fa fa-arrow-circle-o-left" ></span></a> Question</h2>
                    </div>    
                </div>
                <!-- END PAGE TITLE -->                
                <div class="alert alert-success" role="alert" ng-if="successMessage">
                    <a class="close" aria-label="Close" ng-click="hideMessage()">&times;</a>
                    @{{successMessage}}
                </div>
                <div class="alert alert-danger" role="alert" ng-if="error">
                    <a class="close" aria-label="Close" ng-click="hideMessage()">&times;</a>
                    @{{error}}
                </div>                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table datatable-simple" id="question-list">
                                    <thead>
                                        <tr>
                                            <th>Question</th>
                                            <th>option1</th>
                                            <th>option2</th>
                                            <th>option3</th>
                                            <th>option4</th>
                                            <th>answer</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                <!-- PAGE CONTENT WRAPPER -->          
                <!-- POP UP TO ADD OPPORTUNITY -->     
        
<!-- POPUP ENDS -->
@include('questions.add-edit')
@include('popups.delete')
</div>                
@endsection

@push('scripts')
    <script type="text/javascript" src="{{asset('asset/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
    <script type="text/javascript" src="{{asset('asset/js/question.js')}}"></script>
    <script type = "text/javascript" src="{{asset('asset/js/loader.js')}}"></script>
@endpush