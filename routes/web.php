<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/get-interview-questions','QuestionController@getQuestion');
Route::post('/applicant-response','QuestionController@formSubmit');

Route::group(['middleware' => 'auth'], function() {
    Route::resource('question', 'QuestionController');
    Route::get('/get-questions', 'QuestionController@list');
    


});


Route::get('/dashboard',function(){
	return view('userquestion.dashboard');
});
    Route::get('logout',function(){
    	 Auth::logout();
  		return redirect('/login');
    });
