var app = angular.module('userApp', ['ngSanitize']);
app.controller('UserController',  function ($scope,  $anchorScroll, $http, $compile) {
    $scope.init = function(){
        $scope.getusers();
        $scope.listing_form = {};
        $scope.getBusinessUnits();
        $scope.getRoles();
        $scope.users = [];
        $scope.roles = [];
        $scope.user_form = {};
        $scope.bUnits = {}
        $scope.successMessage= "";
        $scope.modalErrors= [];
        $scope.error= "";
        $scope.selectedDelete;
        $scope.selectedUser;
    };
    
    $scope.userUpdates = function(){
        var url="user-updates";
        $http.get(url)
        .then(function (response) {
            if(response.status == 201) {
                $scope.getusers();
                $scope.successMessage = response.data.message;
                $anchorScroll();             
            }else{
                $scope.modalErrors = response.data.error.message;
            }
        });        
    };    
    
    $scope.getusers = function() {

      $('#user-list').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            "order": [[0, "asc" ]],
            "language":{
                "processing": "<div raw-ajax-busy-indicator class='bg_load text-center'>\n\
                <img src='asset/img/ajax.gif' style='margin-top:20%;' ></div>"
            },
            ajax: {url:'get-users',
                    data: $scope.listing_form
                },
            columns: [
              {data:'first_name', name: 'first_name'},  
              {data:'last_name', name: 'last_name'},
              {data: 'email', name: 'email',orderable:false},
              {data: 'role.name', name: 'role.name'},
              //{data: 'phone', name: 'phone',orderable:false},
              {data: 'status', name: 'status'},
              {data: 'buName', name: 'buName'},
              //{data: 'actions',name:'actions',searchable:false,orderable:false},
            ],
            createdRow: function (row, data, dataIndex) {
                $compile(angular.element(row).contents())($scope);
            }
        });

    };


    $scope.openModal =  function () {
        $scope.modalErrors= [];
        $scope.user_form = {};
        $('#mb-user').modal('show');
    };
    $scope.getRoles =  function() {
        $http.get('api/get-active-roles')
        .then(function (response) {
            $scope.roles = response.data.data;
        });
    };
    $scope.editUser = function(id){
        $scope.modalErrors= [];
        var url = 'api/get-users/'+id;
        $http.get(url)
        .then(function (response) {
            $scope.user_form = response.data.data;
            $('#mb-user').modal('show');
        });
    };
    $scope.deleteUser = function(id){
        $('#message-box-danger').addClass("open");
        $scope.selectedDelete = id;
    };
    $scope.deleteConfirmed = function(){
        var url = 'api/get-users/'+$scope.selectedDelete;
        $http.delete(url)
        .then(function (response) {
            if (response.status == 201) {
                $scope.getusers();
                $scope.successMessage = response.data.message;
                $anchorScroll();
            }
        }).catch(function (e){
            
        });
        $('#message-box-danger').removeClass("open");
    };
    $scope.submitUser =  function () {
        $http.post('/user/add-user',$scope.user_form).then(function (response) {
            if (response.status == 201) {
                $('#mb-user').modal('hide');
                $scope.user_form = {};
                $scope.getusers();
                $scope.successMessage = response.data.message;
                $anchorScroll();
            } else {
                $scope.modalErrors = response.data.error;
            }
        });

    }
    $scope.hideMessage = function(){
      
        if($scope.modalErrors){
            delete $scope.modalErrors;
        }
        if($scope.successMessage){
            delete $scope.successMessage;
        }
        if($scope.error){
            delete $scope.error;
        }
    }
    $scope.inviteUser = function(id){
        $('#message-box-invite').addClass("open");
        $scope.selectedUser = id;
    }
    $scope.inviteUserConfirmed = function(id){
        var url = 'invite-user/'+$scope.selectedUser;
        $http.get(url)
        .then(function (response) {
            $scope.successMessage = response.data.message;
            $('#message-box-invite').removeClass("open");
        });
    }

    $scope.getBusinessUnits = function(id){
        var url = '/users/get-business-units';
        $http.get(url)
        .then(function (response) {
            $scope.listing_form.business_unit_id = response.data.data.user_bu;
            $scope.bUnits = response.data.data.bUnits;
            $scope.getusers();
        });
    };
    $scope.init();
});
