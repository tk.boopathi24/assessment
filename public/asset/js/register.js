/*

 This script holds the actions related to Application registration

*/


var app = angular.module('registrationApp', []);
app.controller('registrationController',function($scope, $http, $window) {



  //Method to show the errors
  $scope.getError = function(field){
    if($scope.errors[field]){
      return $scope.errors[field][0];
    }
  }
  

  

  //Applicant form Submit
  $scope.applicantFormSubmit = function(){
    var url = 'api/user/register';
    var errors =[];

    if($scope.applicantForm.name==null || $scope.applicantForm.name=='undefined' ){
      errors.push({fieldname:"name",error:["please enter the first name"]});
      
    }
    if($scope.applicantForm.email==null || $scope.applicantForm.email=='undefined' ){
        errors.push({fieldname:"email",error:["please enter the email address"]});
    }else{
      var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if(!$scope.applicantForm.email.match(mailformat)){
        errors.push({fieldname:"email",error:["email address invalid"]});
      }
    }
    if($scope.applicantForm.role==null || $scope.applicantForm.role=='undefined' ){
        errors.push({fieldname:"role",error:["please choose role"]});
    }    
    if($scope.applicantForm.password==null || $scope.applicantForm.password=='undefined' ){
        errors.push({fieldname:"password",error:["please enter password"]});
    }else{
      if($scope.applicantForm.password != $scope.applicantForm.password_confirmation ){
        errors.push({fieldname:"password",error:["password mismatch"]});
      }  
    }        

    if (errors != '') {
    var objectMap = arrayToObj(errors,function (item) {
                           return {key:item.fieldname,value:item.error};
                        });
      $scope.errors =objectMap;
      return false;
    }


    return $http.post(url,$scope.applicantForm).then(function(response) {
        if(response.status==200){
            $window.location.href='/home';
        }
        
    }).catch(function(e){
      console.log(e.data.errors);
       $scope.errors = e.data.errors;
    });

  }

  function arrayToObj (array, fn) {
    var obj = {};
    var len = array.length;
    for (var i = 0; i < len; i++) {
        var item = fn(array[i], i, array);
        obj[item.key] = item.value;
    }
    return obj;
};


  
  $scope.init = function(){
    $scope.errors = {};
    $scope.applicantForm={};

  }

  $scope.init();

});
