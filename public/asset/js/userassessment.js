/*

 This script holds the actions related to Application registration

*/


var app = angular.module('assessmentFormApp', []);
app.controller('assessmentFormController',function($scope, $http, $window) {


//Applicant form Submit
  $scope.submitAnswer = function(){
    console.log();
    var url = '/applicant-response';
    var errors =[];
   return $http.post(url,$scope.questionsForm).then(function(response) {
        if(response.status==200){
          if(response.data[0] == 'FAILED'){
              $scope.popupHeader="Better luck next time!!";
              $scope.popupContent="click ok to redirect dashboard";            
          }else{
              $scope.popupHeader="Congratulation";
              $scope.popupContent="You are Passsed";            
          }
          $('#mb-status').modal('show');

            
        }
        
    }).catch(function(e){
      console.log(e.data.errors);
       $scope.errors = e.data.errors;
    });

  }
  $scope.logOut = function(){
      $("#mb-signout").modal('show');
  }
  $scope.redirectHome =function(){
    $window.location.href='/dashboard';
  }

  $scope.getQuestions = function(){
    var url="/get-interview-questions"
    $http.get(url).then(function (response) {
      if(response.status ==200){
        $scope.assessmentForm = response.data;
      }
    }).catch(function(e){
      console.log(e.data.errors);
       $scope.errors = e.data.errors;
    });
  }

  
  $scope.init = function(){
    $scope.errors = {};
    $scope.getQuestions();
    $scope.assessmentForm={};
    $scope.questionsForm={};
    $scope.questionsForm.answer=[];
    $scope.popupHeader="";
    $scope.popupContent="";

  }

  $scope.init();

});
