var app = angular.module('questionApp', ['ngSanitize']);
app.controller('questionController', function ($scope, $http,$compile, $anchorScroll) {
    $scope.init = function(){
        $scope.getquestions();
        $scope.questions = [];
        $scope.questionsForm= {};
        $scope.successMessage= "";
        $scope.modalError= [];
        $scope.error ="";
    };
    $scope.logOut =  function () {



        $('#mb-signout').modal('show');
    }
    $scope.openModal =  function () {
        $('#mb-addquestion').modal('show');
        
        $scope.questionsForm = {};
        $scope.modalErrors= [];
    };
    
    //Method to show the errors

    $scope.getquestions = function() {
      $('#question-list').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            "order": [[1, "desc" ]],
            "language":{
                "processing": "<div raw-ajax-busy-indicator class='bg_load text-center'>\n\
                <img src='asset/img/ajax.gif' style='margin-top:20%;' ></div>"
            },
            ajax: '/get-questions',
            columns: [
              {data:'question', name: 'question'},
              {data: 'option1', name: 'option1'},
              {data: 'option2', name: 'option2'},
              {data: 'option3', name: 'option3'},
              {data: 'option4', name: 'option4'},
              {data: 'answer', name: 'answer'},
              {data: 'actions',name:'actions',searchable:false,orderable:false},
            ],
            createdRow: function (row, data, dataIndex) {
                $compile(angular.element(row).contents())($scope);
            }
        });

    };    
    
    $scope.editQuestion = function(id){
        $scope.questionsForm = {};
        $scope.modalErrors= [];        
        var url = '/question/'+id;
        $http.get(url)
        .then(function (response) {
            $scope.questionsForm = response.data;
            $('#mb-addquestion').modal('show');
        });
    };
    $scope.deleteQuestion = function(id){
        $('#message-box-danger').addClass("open");
        $scope.selectedDelete = id;
    };
    $scope.deleteConfirmed = function(){
        $scope.error = "";
        $scope.successMessage = "";
        var url = '/question/'+$scope.selectedDelete;
        $http.delete(url)
        .then(function (response) {
            if (response.status == 200) {
                $scope.getquestions();
                $scope.successMessage = response.data.message;
                $anchorScroll();
            }else{
                $scope.error = response.data.error.message;
            }
        });
        $('#message-box-danger').removeClass("open");
    };
    $scope.submitQuestion =  function () {

 var errors =[];

    if($scope.questionsForm.question==null || $scope.questionsForm.question=='undefined' ){
      errors.push("please enter the question");
      
    }
    if($scope.questionsForm.option1==null || $scope.questionsForm.option1=='undefined' ){
      errors.push("please enter the answer 1");
    }    
    if($scope.questionsForm.option2==null || $scope.questionsForm.option2=='undefined' ){
      errors.push("please enter the answer 2");
    }    
        if($scope.questionsForm.option3==null || $scope.questionsForm.option3=='undefined' ){
      errors.push("please enter the answer 3");
    }    
        if($scope.questionsForm.option4==null || $scope.questionsForm.option4=='undefined' ){
      errors.push("please enter the answer 4");
    }    
        if($scope.questionsForm.answer==null || $scope.questionsForm.answer=='undefined' ){
      errors.push("please choose one correct answer");
    }        
    
    if (errors != '') {
      $scope.modalErrors =errors;
      return false;
    }    

        $http.post('/question',$scope.questionsForm).then(function (response) {
          if (response.status == 200) {
                $('#mb-addquestion').modal('hide');
                $scope.questionsForm = {};
                $scope.getquestions();
                $scope.successMessage = response.data.message;
                $anchorScroll();
            }
        }).catch(function(e){
            $scope.modalErrors = e.data.errors.name;
        });
    }
 

    $scope.hideMessage = function(){
        if($scope.modalErrors){
            delete $scope.modalErrors;
        }
        if($scope.successMessage){
            delete $scope.successMessage;
        }
    }
    $scope.init();

    $(".mb-control-close").on("click",function(){
       $(this).parents(".message-box").removeClass("open");
       return false;
    });    
});